# JS-Héritage

Un projet JS pour la P9 de Simplon Lyon pour voir l'héritage en POO

## How To Use 
1. Git clone le projet puis `cd js-heritage`
2. `npm install` pour mettre les dépendances du projet
3. `npm run dev` pour lancer la compilation webpack

## Exercices Animal / Food
### I. Les classes Animal et Food
1. Créer une classe Food dans son propre fichier
2. La Food aura comme propriété un type en chaîne de caractère (vegetal ou meat ou fish) et un calories en number
3. Faire la méthode eat de Animal pour faire que l'animal réduise sa faim du nombre de calories de la food qu'il mange
4. Faire qu'il ne mange que si le type de food correspond à sa diet (si herbivorous il mange que du vegetal, si omnivorous il mange de tout, si carnivorous il mange que de la meat et du poisson)
### II. Heritage Mini exo
1. Dans la classe Dog, faire une méthode bark() qui fera un return de bork bork bork
2. Faire également une méthode fetch(thing:Object) qui fera un console log disant que le chien va chercher le truc en concatenant le truc en question dans le console log
3. Faire un nouveau fichier croquette.js qui contiendra une classe Croquette qui héritera de la classe Food et qui aura une méthode supplémentaire flip() qui renverse les croquettes (un console log) et qui réduit les calories de l'instance actuelle de 20
4. Dans l'index, lancer la méthode fetch pour que le chien aille chercher l'instance de food qu'il y a dans banana et voir ce que ça met comme résultat
5. Dans la classe Food, rajouter une méthode toString qui fera un return de "some food" puis recharger la page pour voir comment a évolué le fetch

Faire en sorte que quand le chien mange des croquettes, il les renverse avant de les manger
* Redéfinir la méthode eat dans la classe Dog
* Faire appel à la méthode eat du parent dans la méthode eat du chien
il faut pour ça utiliser le super.eat
* Dans la méthode eat du chien, vérifier si la food qu'on lui donne c'est des croquettes
utiliser un if avec un instanceof dedans
* Faire que si c'est des croquettes, on déclenche les renverse


## Exercice RPG
### I. Les classes Character et Item
1. Dans le src, créer un nouveau dossier exo, dans ce dossier exo, créer un fichier character.js avec une classe Character à l'intérieur
![Diagramme de la classe Character](./uml/character.jpg)
Par défaut, l'inventaire sera un tableau vide (il ne fera pas parti des arguments du constructor)
2. Faire la méthode takeItem qui va juste prendre l'argument item et l'ajouter dans l'inventaire du personnage (ajouter un truc dans un tableau....)
(on peut laisser le useItem de côté pour le moment, vu qu'on a pas encore fait la classe Item)
3. Dans le dossier exo, créer une nouvelle classe Item
![Diagramme de la classe Item](./uml/item.jpg)
La méthode useOn pour le moment va juste faire un console log disant que "Using [nom de l'item] on [nom du personnage]"
4. Faire la méthode useItem du Character qui va en gros prendre l'item à l'index donné dans sa propriété inventaire puis déclencher la méthode useOn de l'Item sur le Character lui même
5. Modifier la méthode takeItem pour faire en sorte que le personnage ne puisse prendre que des élément étant des instances de item
Bonus : Rajouter une propriété amount sur l'item qui déterminera combien de fois on peut l'utiliser, puis faire que la méthode useOn déminue la valeur de amount, puis faire que la méthode useItem supprime l'item du sac s'il n'a plus d'utilisation possible

### II. Héritage RPG
1. Créer une classe HealthPotion qui hérite de la classe Item, qui aura une propriété effect en number
![Diagramme de la classe Potion](./uml/potion.jpg)
2. Redéfinir la méthode useOn dans HealthPotion qui fait qu'elle exécutera le useOn de son parent puis qu'elle additionnera la propriété effect de la potion à la propriété health du personnage

![Diagramme de l'appli entière](./uml/rpg.jpg)