/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/rpg.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/exo/character.js":
/*!******************************!*\
  !*** ./src/exo/character.js ***!
  \******************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./item */ "./src/exo/item.js");


class Character {
    /**
     * 
     * @param {number} health 
     * @param {string} name 
     * @param {number} armor 
     * @param {number} exp 
     */
    constructor(health, name, armor, exp) {
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = [];
    }
    /**
     * La méthode takeItem permet à un personnage de "ramasser" un item
     * et de le mettre dans son sac (son inventory)
     * @param {Item} item 
     */
    takeItem(item) {
        //On ne met dans l'inventaire que les instance d'Item et rien d'autre
        if (item instanceof _item__WEBPACK_IMPORTED_MODULE_0__["Item"]) {
            this.inventory.push(item);
        } else {
            console.log("I can only pick items!")
        }

    }
    /**
     * Cette méthode fera qu'un personnage prendra un Item dans son
     * sac puis utilisera l'item sur lui même.
     * @param {number} index l'index de l'item à utiliser
     */
    useItem(index) {
        //On récupère l'item qui est dans l'inventaire à l'index voulu
        let choosedItem = this.inventory[index];
        //On utilise la méthode useOn de l'item en donnant l'instance
        //actuelle de personnage comme cible de l'item
        choosedItem.useOn(this);
    }
}

/***/ }),

/***/ "./src/exo/item.js":
/*!*************************!*\
  !*** ./src/exo/item.js ***!
  \*************************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./character */ "./src/exo/character.js");


class Item{
    /**
     * @param {String} name 
     */
    constructor(name){
        this.name = name;

    }
    /**
     * Méthode qui utilise l'item sur un personnage donné
     * @param {Character} character Le personnage sur lequel on utilise l'item
     */
    useOn(character){
        /* Ici, on fait une concaténation en utilisant à la fois le
        name de l'item qu'on utilise (this.name) et en même temps le name
        du personnage sur lequel on utilise l'item (character.name)
         */
        console.log("Using " + this.name + " on " + character.name);
        
    }       
}

/***/ }),

/***/ "./src/rpg.js":
/*!********************!*\
  !*** ./src/rpg.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _exo_character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./exo/character */ "./src/exo/character.js");
/* harmony import */ var _exo_item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./exo/item */ "./src/exo/item.js");




let character1 = new _exo_character__WEBPACK_IMPORTED_MODULE_0__["Character"](100, 'Michel', 0, 0);

let fork = new _exo_item__WEBPACK_IMPORTED_MODULE_1__["Item"]('fork');

character1.takeItem(fork);

character1.useItem(0);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2V4by9jaGFyYWN0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2V4by9pdGVtLmpzIiwid2VicGFjazovLy8uL3NyYy9ycGcuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBOEI7O0FBRXZCO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxLQUFLO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QiwwQ0FBSTtBQUNoQztBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUMzQ0E7QUFBQTtBQUFBO0FBQXdDOztBQUVqQztBQUNQO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFVBQVU7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSztBQUNBLEM7Ozs7Ozs7Ozs7OztBQ3RCQTtBQUFBO0FBQUE7QUFBNEM7QUFDVjs7O0FBR2xDLHFCQUFxQix3REFBUzs7QUFFOUIsZUFBZSw4Q0FBSTs7QUFFbkI7O0FBRUEsc0IiLCJmaWxlIjoiZXhlcmNpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9ycGcuanNcIik7XG4iLCJpbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vaXRlbVwiO1xuXG5leHBvcnQgY2xhc3MgQ2hhcmFjdGVyIHtcbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaGVhbHRoIFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIFxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBhcm1vciBcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gZXhwIFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGhlYWx0aCwgbmFtZSwgYXJtb3IsIGV4cCkge1xuICAgICAgICB0aGlzLmhlYWx0aCA9IGhlYWx0aDtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5hcm1vciA9IGFybW9yO1xuICAgICAgICB0aGlzLmV4cCA9IGV4cDtcbiAgICAgICAgdGhpcy5pbnZlbnRvcnkgPSBbXTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogTGEgbcOpdGhvZGUgdGFrZUl0ZW0gcGVybWV0IMOgIHVuIHBlcnNvbm5hZ2UgZGUgXCJyYW1hc3NlclwiIHVuIGl0ZW1cbiAgICAgKiBldCBkZSBsZSBtZXR0cmUgZGFucyBzb24gc2FjIChzb24gaW52ZW50b3J5KVxuICAgICAqIEBwYXJhbSB7SXRlbX0gaXRlbSBcbiAgICAgKi9cbiAgICB0YWtlSXRlbShpdGVtKSB7XG4gICAgICAgIC8vT24gbmUgbWV0IGRhbnMgbCdpbnZlbnRhaXJlIHF1ZSBsZXMgaW5zdGFuY2UgZCdJdGVtIGV0IHJpZW4gZCdhdXRyZVxuICAgICAgICBpZiAoaXRlbSBpbnN0YW5jZW9mIEl0ZW0pIHtcbiAgICAgICAgICAgIHRoaXMuaW52ZW50b3J5LnB1c2goaXRlbSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkkgY2FuIG9ubHkgcGljayBpdGVtcyFcIilcbiAgICAgICAgfVxuXG4gICAgfVxuICAgIC8qKlxuICAgICAqIENldHRlIG3DqXRob2RlIGZlcmEgcXUndW4gcGVyc29ubmFnZSBwcmVuZHJhIHVuIEl0ZW0gZGFucyBzb25cbiAgICAgKiBzYWMgcHVpcyB1dGlsaXNlcmEgbCdpdGVtIHN1ciBsdWkgbcOqbWUuXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGluZGV4IGwnaW5kZXggZGUgbCdpdGVtIMOgIHV0aWxpc2VyXG4gICAgICovXG4gICAgdXNlSXRlbShpbmRleCkge1xuICAgICAgICAvL09uIHLDqWN1cMOocmUgbCdpdGVtIHF1aSBlc3QgZGFucyBsJ2ludmVudGFpcmUgw6AgbCdpbmRleCB2b3VsdVxuICAgICAgICBsZXQgY2hvb3NlZEl0ZW0gPSB0aGlzLmludmVudG9yeVtpbmRleF07XG4gICAgICAgIC8vT24gdXRpbGlzZSBsYSBtw6l0aG9kZSB1c2VPbiBkZSBsJ2l0ZW0gZW4gZG9ubmFudCBsJ2luc3RhbmNlXG4gICAgICAgIC8vYWN0dWVsbGUgZGUgcGVyc29ubmFnZSBjb21tZSBjaWJsZSBkZSBsJ2l0ZW1cbiAgICAgICAgY2hvb3NlZEl0ZW0udXNlT24odGhpcyk7XG4gICAgfVxufSIsImltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gXCIuL2NoYXJhY3RlclwiO1xuXG5leHBvcnQgY2xhc3MgSXRlbXtcbiAgICAvKipcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lKXtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcblxuICAgIH1cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgdXRpbGlzZSBsJ2l0ZW0gc3VyIHVuIHBlcnNvbm5hZ2UgZG9ubsOpXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBMZSBwZXJzb25uYWdlIHN1ciBsZXF1ZWwgb24gdXRpbGlzZSBsJ2l0ZW1cbiAgICAgKi9cbiAgICB1c2VPbihjaGFyYWN0ZXIpe1xuICAgICAgICAvKiBJY2ksIG9uIGZhaXQgdW5lIGNvbmNhdMOpbmF0aW9uIGVuIHV0aWxpc2FudCDDoCBsYSBmb2lzIGxlXG4gICAgICAgIG5hbWUgZGUgbCdpdGVtIHF1J29uIHV0aWxpc2UgKHRoaXMubmFtZSkgZXQgZW4gbcOqbWUgdGVtcHMgbGUgbmFtZVxuICAgICAgICBkdSBwZXJzb25uYWdlIHN1ciBsZXF1ZWwgb24gdXRpbGlzZSBsJ2l0ZW0gKGNoYXJhY3Rlci5uYW1lKVxuICAgICAgICAgKi9cbiAgICAgICAgY29uc29sZS5sb2coXCJVc2luZyBcIiArIHRoaXMubmFtZSArIFwiIG9uIFwiICsgY2hhcmFjdGVyLm5hbWUpO1xuICAgICAgICBcbiAgICB9ICAgICAgIFxufSIsImltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gXCIuL2V4by9jaGFyYWN0ZXJcIjtcbmltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9leG8vaXRlbVwiO1xuXG5cbmxldCBjaGFyYWN0ZXIxID0gbmV3IENoYXJhY3RlcigxMDAsICdNaWNoZWwnLCAwLCAwKTtcblxubGV0IGZvcmsgPSBuZXcgSXRlbSgnZm9yaycpO1xuXG5jaGFyYWN0ZXIxLnRha2VJdGVtKGZvcmspO1xuXG5jaGFyYWN0ZXIxLnVzZUl0ZW0oMCk7Il0sInNvdXJjZVJvb3QiOiIifQ==