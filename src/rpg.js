import { Character } from "./exo/character";
import { Item } from "./exo/item";


let character1 = new Character(100, 'Michel', 0, 0);

let fork = new Item('fork');

character1.takeItem(fork);

character1.useItem(0);