import  "./scss/index.scss";
import { Food } from "./food";
import { Animal } from "./animal";
import { Dog } from "./dog";
import { Croquette } from "./croquette";


let monkey = new Animal(4, 'omnivorous', 100);

let banana = new Food('vegetal', 75);


monkey.eat(banana);
console.log(monkey.hunger);

let dog = new Dog('fido', 'corgi');

console.log(dog.bark());

dog.fetch(dog);

let croquettes  = new Croquette('meat', 200);
croquettes.flip()
console.log(croquettes);

dog.eat(croquettes);

console.log(dog);
