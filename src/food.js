export class Food {
    /**
     * 
     * @param {string} type Types possibles : 'vegetal', 'meat' ou 'fish'
     * @param {number} calories Le nombre de calories de la nourriture
     */
    constructor(type, calories) {
        this.type = type;
        this.calories = calories;
    }
    
    toString() {
        return 'some food';
    }
}