import { Animal } from "./animal";
import { Croquette } from "./croquette";


export class Dog extends Animal {
    /**
     * @param {string} name 
     * @param {string} breed 
     */
    constructor(name, breed, hunger = 50) {
        /**
         * Une classe qui hérite d'une autre classe doit dans son constructor
         * faire appel au constructor de son parent avant tout autre chose
         */
        super(4, 'omnivorous', hunger);
        this.name = name;
        this.breed = breed;
    }
    bark() {
        return 'bork bork';
    }
    fetch(thing) {
        console.log('The dog fetch '+thing);
        
    }
    toString() {
        return super.toString() + ' : a dog';
    }
    eat(food) {
        if(food instanceof Croquette) {
            food.flip();
        }
        
        super.eat(food);
    }
}
