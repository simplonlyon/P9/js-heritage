import { Food } from "./food";

export class Animal {
    constructor(pawNb, diet, hunger = 0) {
        this.isAlive = true;
        this.pawNb = pawNb;
        this.diet = diet;
        this.hunger = hunger;
    }
    /**
     * @param {Food} food 
     */
    eat(food) {
        
        if (this.diet === 'herbivorous' && food.type === 'vegetal') {
            this.hunger -= food.calories;
        }

        if (this.diet === 'carnivorous' && (food.type === 'meat' || food.type === 'fish')) {
            this.hunger -= food.calories;
        }


        if (this.diet === 'omnivorous') {
            this.hunger -= food.calories;
        }
    }
    play() {
        console.log("I'm playing...")
    }

    toString() {
        return super.toString() + ' : an animal'
    }
}