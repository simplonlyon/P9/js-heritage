import { Item } from "./item";

export class Character {
    /**
     * 
     * @param {number} health 
     * @param {string} name 
     * @param {number} armor 
     * @param {number} exp 
     */
    constructor(health, name, armor, exp) {
        this.health = health;
        this.name = name;
        this.armor = armor;
        this.exp = exp;
        this.inventory = [];
    }
    /**
     * La méthode takeItem permet à un personnage de "ramasser" un item
     * et de le mettre dans son sac (son inventory)
     * @param {Item} item 
     */
    takeItem(item) {
        //On ne met dans l'inventaire que les instance d'Item et rien d'autre
        if (item instanceof Item) {
            this.inventory.push(item);
        } else {
            console.log("I can only pick items!")
        }

    }
    /**
     * Cette méthode fera qu'un personnage prendra un Item dans son
     * sac puis utilisera l'item sur lui même.
     * @param {number} index l'index de l'item à utiliser
     */
    useItem(index) {
        //On récupère l'item qui est dans l'inventaire à l'index voulu
        let choosedItem = this.inventory[index];
        //On utilise la méthode useOn de l'item en donnant l'instance
        //actuelle de personnage comme cible de l'item
        choosedItem.useOn(this);
    }
}